"""
functions to process the results of a check and save them
"""
import json
import os

from typing import List, Tuple

from checker.result import Result

__this_module_directory = os.path.dirname(__file__) + '/'


def save_results(raw_results):
    results = _load_existing_results()

    # process results
    new_results = _process_results(raw_results)

    results.append(new_results)
    results_json = json.dumps(results, sort_keys=True, indent=4)
    # print(results_json) DEBUG

    with open(__this_module_directory + '../../data/check_results.json', 'w') as results_file:
        results_file.write(results_json)

def _load_existing_results():
    try:
        with open(__this_module_directory + '../../data/check_results.json', 'r') as results_file:
            results_json = results_file.read()
        return json.loads(results_json)
    except FileNotFoundError:
        return []

def _process_results(raw_results):
    # Files that are not I DB have nothing to do in report. They triggered a
    # message, so the user should update their DB. But before, it's usefull to
    # do a check (to not ignore maliciously changed files)
    check_files_results = list(filter(_all_except_not_in_DB, raw_results))

    number_of_files = len(check_files_results)
    files_changed = _changed_files(check_files_results)
    files_missing = _missing_files(check_files_results)

    return {
        "date": _get_current_date_and_time(),
        "number_of_files_checked": number_of_files,
        "files_changed": _map_to_str(files_changed),
        "files_missing": _map_to_str(files_missing)
    }



def _all_except_not_in_DB(result):
    result_type, _ = result
    return result_type != Result.not_in_DB


# Type hints test
def _changed_files(results: List[Tuple[Result, str]]):
    changed_results = _filter_results_by_type(results, Result.hash_mismatch)
    return _results_to_files(changed_results)


def _missing_files(results):
    missing_results = _filter_results_by_type(results, Result.file_not_found)
    return _results_to_files(missing_results)


def _filter_results_by_type(results, res_type):
    def is_result_of_type(result):
        type, _ = result
        return type == res_type
    return filter(is_result_of_type, results)


def _results_to_files(results):
    def extract_file(result):
        a, file = result
        return file
    return map(extract_file, results)


def _map_to_str(map_object):
    return str(list(map_object))

def _get_current_date_and_time():
    from time import gmtime, strftime
    return strftime("%Y-%m-%d %H:%M:%S", gmtime())
