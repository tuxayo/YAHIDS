# Python 3
import hashlib
import configparser
import json
import os

from functools import partial

from checker.result import Result
from checker.result_saver import save_results

__this_module_directory = os.path.dirname(__file__) + '/'


def check_all_files():
    # load hash database
    with open(__this_module_directory + '../../data/hash_database.json', 'r') as hashes_file:
        hashes_json = hashes_file.read()
    hash_database = json.loads(hashes_json);

    # check and save
    hasher = hashlib.new(_get_hash_algorithm())
    check_one_file_in_DB = partial(_check_one_file, hash_database, hasher)
    results = map(check_one_file_in_DB, _get_files_to_check())
    return save_results(results)


def update_DB():
    """
    Rewrite ALL the hash DB from the list of files in config.
    Usefull when setting the app or when changing the hash algorithm
    """
    new_DB = _hash_files(_get_files_to_check(), _get_hash_algorithm())
    _write_hash_DB(new_DB)
    print('File database has been sucessfully updated')


def _get_files_to_check():
    config = configparser.ConfigParser()
    config.read(__this_module_directory + '../../config.ini')
    try:
        return json.loads(config['YAHIDS']['files_to_check'])
    except json.JSONDecodeError as JSON_error:
        import_error = ImportError('Error when loading list of files to check, '
                                   'please check config file')
        raise import_error from JSON_error


def _get_hash_algorithm():
    config = configparser.ConfigParser()
    config.read(__this_module_directory + '../../config.ini')
    return config['YAHIDS']['hash_algorithm']

def _check_one_file(hash_database, hasher, file_path):
    if file_path not in hash_database:
        return (Result.not_in_DB, file_path)

    original_digest = hash_database[file_path]
    try:
        current_digest = _hash(file_path, hasher)
    except FileNotFoundError:
        return (Result.file_not_found, file_path)

    if current_digest == original_digest:
        return (Result.ok, file_path)
    else:
        return (Result.hash_mismatch, file_path)


def _write_hash_DB(hash_DB):
    hash_DB_json = json.dumps(hash_DB, sort_keys=True, indent=4)
    with open(__this_module_directory + "../../data/hash_database.json", 'w') as hashes_file:
        hashes_file.write(hash_DB_json)


def _hash_files(file_list, hash_algorithm):
    hasher = hashlib.new(hash_algorithm)
    file_hashes = {}
    file_not_found = False
    for file_path in file_list:
        try:
            digest = _hash(file_path, hasher)
            file_hashes[file_path] = digest
        except FileNotFoundError:
            print('log_error: file not found: "' + file_path + '"')
            file_not_found = True

    if file_not_found:
        raise FileNotFoundError("One or more files to check have not been found, "
                                "see previous error messages")
    return file_hashes


def _hash(file_path, hasher):
    BLOCKSIZE = 2**20*5 # 5 MiB
    with open(file_path, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()
