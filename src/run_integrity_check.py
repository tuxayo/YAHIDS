#!/usr/bin/env python3

from checker.checker import check_all_files

if __name__ == '__main__':
    check_all_files()
